
local rainbow_warriors = {}
local rainbow_sounds = {}

local add_warrior = function(warrior)
	table.insert(rainbow_warriors, warrior:get_player_name())
	local sound = {}
	sound.owner = warrior:get_player_name()
	sound.handler = minetest.sound_play("rainbowpi_stream", {
		object = warrior,
		max_hear_distance = 6,
		loop = true,
	})
	table.insert(rainbow_sounds, sound)
end

local remove_warrior = function(warrior_name)
	if #rainbow_warriors == 0 then
		return
	end

	for k, warrior_name_ in ipairs(rainbow_warriors) do
		if warrior_name_ == warrior_name then
			table.remove(rainbow_warriors, k)
			return
		end
	end
end

local remove_sound = function(warrior_name)
	if #rainbow_sounds == 0 then
		return
	end

	for k, sound in ipairs(rainbow_sounds) do
		if sound.owner == warrior_name then
			minetest.sound_stop(sound.handler)
			table.remove(rainbow_sounds, k)
			return
		end
	end
end

local is_warrior_exist = function(warrior_name)
	if #rainbow_warriors == 0 then
		return false
	end

	for k, warrior_name_ in ipairs(rainbow_warriors) do
		if warrior_name_ == warrior_name then
			return true
		end
	end	

	return false
end

local random_minus = function()
	if math.random() < 0.51 then
		return -1
	end
	return 1
end

local random_texture = function()
	return "rainbowpi_c" .. math.random(1,10) .. ".png"
end

local create_particle = function(pos, dir)
	local multiply = {x=math.random() + 5, y=math.random() + 5, z=math.random() + 5}
	local velocity_ = vector.multiply(dir, multiply)

	minetest.add_particle({
		pos = {x=pos.x, y=pos.y, z=pos.z},
		velocity = velocity_,
		acceleration = {x=0, y=-3, z=0},
		expirationtime = 0.9,
		size = (math.random() + 0.01) / 2,
		collisiondetection = true,
		glow = 14,
		texture = random_texture()
	})
end

local the_flowers_will_grow = function(player)
	local pos = table.copy(player:getpos())
	local dir = player:get_look_dir()
	
	pos.y = pos.y + 0.6
	local range = 4

	for i = 1, range do
		create_particle(pos, dir)
	end
end

minetest.register_globalstep(function(dtime)
	if #minetest.get_connected_players() == 0 then
		return
	end

	for k, player in ipairs(minetest.get_connected_players()) do
		if is_warrior_exist(player:get_player_name()) then
			for i = 1, 3 do
				the_flowers_will_grow(player)
			end
		end
	end
end)

local colorful_bucket_on_use = function(user)
	if user == nil or user:get_player_name() == nil then
		return
	end

	if is_warrior_exist(user:get_player_name()) then
		remove_warrior(user:get_player_name())
		remove_sound(user:get_player_name())
	else
		add_warrior(user)
	end
end

minetest.register_craftitem("rainbowpi:colorful_bucket", {
	description = "Colorful Bucket",
	inventory_image = "rainbowpi_bucket.png",
	stack_max = 1,
	wield_scale = {x = 0.5, y = 0.5, z = 0.5},
	on_use = function(itemstack, user, pointed_thing)
		colorful_bucket_on_use(user)
		return nil
	end
})
